from django.urls import path
from .views import DaftarBukuViews, get_keyWord

urlpatterns = [
    path('', DaftarBukuViews, name = 'daftarbuku'),
    path('KeyWord/<str:key>', get_keyWord, name = 'dataDariKey'),
]